source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.3'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'webpacker'
gem 'foreman'
gem 'jquery-rails'
gem 'font-awesome-sass', '~> 4.7.0'
gem 'devise'
gem 'shopify_api'
gem 'shopify_app'
gem 'resque', :require => 'resque/server'
gem 'bson', '~> 4.0'
gem 'json'
# The active model serializers gem allows you to 
# use serializers to format each object you use in your API response. 
# They are very handy if you want to only expose specific attributes.
# gem 'will_paginate', '~> 3.1.0'
gem 'active_model_serializers'
gem 'kaminari'
# Api-pagination gem combined with the will_paginate gem allows you to paginate the API call, 
# which only exposes a certain amount of jobs for every API call. It allows you to make a call like:
# lookingforme.herokuapp.com/api/v1/jobs?page=2
gem 'api-pagination'
#gem 'activeadmin', github: 'activeadmin'


# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
 gem 'redis'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  # gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry-rails'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # https://github.com/flyerhzm/bullet
  # The Bullet gem is designed to help you increase your application's performance by reducing the number of queries it makes. 
  # It will watch your queries while you develop your application and notify you when you should add eager loading (N+1 queries), 
  # when you're using eager loading that isn't necessary and when you should use counter cache.
  gem 'bullet'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
