class TrackingsController < ApplicationController
  require 'json'
  def tracking_data
    tracking_data = paginate Tracking.all, per_page: 20
    render json: tracking_data
  end

  def index
    tracking_data
  end
end