class PricesController < ApplicationController
  require 'json'
  def price_data
    price_data = Price.all
    render json: price_data
  end

  def index
    price_data
  end
end