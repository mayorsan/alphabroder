class DashboardController < ApplicationController 
  def ng
    @base_url = "/dashboard/ng"
    render :index
  end

  def index 
    redirect_to dashboard_ng_path 
  end

  # callback for auth0
  def callback
    @base_url = "/dashboard/ng"
    render :index
  end
end