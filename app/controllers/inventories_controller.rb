class InventoriesController < ApplicationController
  require 'json'
  # https://scotch.io/tutorials/build-a-restful-json-api-with-rails-5-part-three
  # https://github.com/mislav/will_paginate/wiki
  # https://github.com/michaelbromley/ngx-pagination
  # https://stackoverflow.com/questions/41947338/import-json-data-to-rails
  def inventory_data
    inventory_data = Inventory.paginate(page: params[:page], :per_page => 20)
    render json: inventory_data
  end

  def index
    inventory_data
  end
end