class ProductsController < ApplicationController
  before_action :set_product, only: [:show]
  require 'json'

  def pagination_dict(collection)
    {
      current_page: collection.current_page,
      next_page: collection.next_page,
      total_pages: collection.total_pages,
      total_count: collection.total_count
    }
  end

  # https://medium.com/@rjohnson4444/building-a-paginated-api-7b4f22629b0b
  # Split JSON file into multiple parts.
  # https://realguess.net/tags/mongoexport/
  def product_data
    @products = paginate Product.filtered_data.all, per_page: 20
    # grouped_products = @products.group_by{ |product| product['name']['AP'] }.values
    # @products = grouped_products
    
    # @products = Product.all
    # filtered_product_data = paginate @products.select { |product| product[:images] != nil && product[:name] != nil }, per_page: 20
    render json: JSON.pretty_generate(@products.as_json)
  end

  def index
    if params[:keywords].present?
      @keywords = params[:keywords]
      # product_search_term = ProductSearchTerm.new(@keywords)
      @products = Product.filtered_data.where(
                    
                    # product_search_term.where_clause, 
                    # product_search_term.where_args).order(product_search_term.order)
      render json: JSON.pretty_generate(@products.as_json)
    else
      @products = product_data
    end
  end

  def show
    render json: @product
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit!
    end
end