# class WebhooksController < ApplicationController

#   skip_before_action :verify_authenticity_token
#   skip_before_action :shop_exists
#   skip_around_action :shopify_session

#   before_action :sanitized_params
#   before_action :verify_shopify_webhook
#   before_action :hook

#   rescue_from ActionController::RoutingError do |exception|
#     head :ok
#   end

#   def uninstall
#     case @hook
#     when 'app/uninstall'
#       app_uninstall
#     end
#     head :ok
#   end

#   private

#   def app_uninstall
#     Resque.enqueue(AppUninstallJob, shop_domain)
#   end

#   def verify_shopify_webhook
#     data = request.body.read.to_s
#     digest = OpenSSL::Digest::Digest.new('sha256')
#     calculated_hmac = Base64.encode64(OpenSSL::HMAC.digest(digest, Rails.Application.config.shopify.secret, data)).strip
#     head :unauthorized unless calculated_hmac == hmac
#     request.body.rewind
#   end


#   def sanitized_params
#     @params = params.except(:action, :controller).with_indifferent_access
#   end

#   def hook
#     @hook = request.headers['HTTP_X_SHOPIFY_TOPIC']
#   end

#   def shop_domain
#     request.headers['HTTP_X_SHOPIFY_SHOP_DOMAIN']
#   end

#   def hmac
#     request.headers['HTTP_X_SHOPIFY_HMAC_SHA256']
#   end

# end