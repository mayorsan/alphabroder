class ProductSearchTerm
  attr_reader :where_clause, :where_args, :order

  def initialize(search_term)
    search_term = search_term.downcase
    @where_clause = ""
    @where_args = {}
    build_for_search(search_term) 
  end

  private
    def build_for_search(search_term)
      name_search = "name->>'AP'"
      description_search = "description->>'AP'"
      @where_clause << case_insensitive_search(name_search)
      @where_args[name_search] = starts_with(search_term)

      @where_clause << " OR #{case_insensitive_search(description_search)}"
      @where_args[description_search] = starts_with(search_term)
      @order = "name_search asc"
    end

    def starts_with(search_term)
      search_term + "%"
    end

    def case_insensitive_search(field_name)
      "lower(#{field_name}) like :#{field_name}"
    end
end