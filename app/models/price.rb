class Price < ActiveRecord::Base
  # ActiveRecord::SubclassNotFound: The single-table inheritance mechanism failed to locate the subclass: 'closeout'. 
  # This error is raised because the column 'type' is reserved for storing the class in case of inheritance. 
  # Please rename this column if you didn't intend it to be used for storing the inheritance class or overwrite Price.inheritance_column to use another column for that information.
  # Added to avoid Rails inheritance_column error
  self.inheritance_column = :_type_disabled
end