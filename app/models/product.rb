class Product < ApplicationRecord
  # https://medium.com/@zedalaye/build-crazy-queries-with-activerecord-and-arel-e7bd3b606324
  # http://nandovieira.com/using-postgresql-and-jsonb-with-ruby-on-rails
  # https://blog.carbonfive.com/2016/11/16/rails-database-best-practices/
  # scope :filter_null_images, -> { Product.where.not(images: nil) }
  # scope :filter_null_images_and_null_name, -> { filter_null_images.where.not(name: nil) }
  # scope :filter_sku_ap, -> { filter_no_images_and_no_name.where.not("sku @> ?", { AP: nil }.to_json) }
  # scope :filter, -> { remove_other_attr.where.not("images @> ?", { AP: nil }.to_json) } 
  scope :get_name_attr_with_ap, -> { Product.where("name ?| array[:keys]", keys: ['AP']) }
  # Found product with no image on page 12
  scope :get_image_attr_with_ap, -> { get_name_attr_with_ap.where("images ?| array[:keys]", keys: ['AP']) }
  scope :filtered_data, -> { get_image_attr_with_ap.where("sku ?| array[:keys]", keys: ['AP']) } 

  # scope :group_by_sku, -> { filter.group("sku @>'AP'") }
end