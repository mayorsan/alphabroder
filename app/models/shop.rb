class Shop < ActiveRecord::Base
  include ShopifyApp::SessionStorage

  def base_url
    # domain
  end

  def shopify_session(&blk)
    ShopifyAPI::Session.temp(base_url, token, &blk)
  end

  private 
  def setup_webhooks
    hooks = {
      'app/uninstall' => 'appuninstall',
    }

    hooks.each { |topic, action| make_webhook(topic, action) }
  end

  def make_webhook(topic, action)
    shopify_session {
      ShopifyAPI::Webhook.create({topic: topic, address: HOOK_ADDRESS + action, format: 'json'})
    }
  end
end
