import { Component, OnInit } from "@angular/core";
import template              from "./notifications.html";

@Component({
  selector: 'ab-notifications',
  template: template
})
export class NotificationsComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {}
}