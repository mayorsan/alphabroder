import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import template from './auth-navbar.html';

@Component({
  selector: 'ab-auth-nav',
  template: template
})
export class AuthNavbarComponent {

  constructor(
    private auth: AuthService
  ) {
    //auth.handleAuthentication();
  }

  login() {
    this.auth.login();
  }

  logout() {
    this.auth.logout()
  }

  isAuthenticated() {
    this.auth.isAuthenticated();
  }
}