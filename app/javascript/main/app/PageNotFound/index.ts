import { Component } from '@angular/core';
import { Location }  from '@angular/common';
import template      from './page-not-found.html';

@Component({
  selector: 'ab-page-not-found',
  template: template
})
export class PageNotFoundComponent { 

  constructor(
    private location: Location
  ) {}

  goBack(): void {
    this.location.back();
  }
}