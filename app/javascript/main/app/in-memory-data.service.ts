import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {

  //Finish it up with this doc - https://angular.io/tutorial/toh-pt6
  createDb() {
    const products = [
      { id: 1, categoryId: 1, name: 'Sweat Shirt', priceRange: '$15.99 - $99.99', description: 'Sexy Sweat shirt from the 2017 Collection', imageUrl: 'https://images.evo.com/imgp/700/101847/476287/poler-state-crew-neck-sweatshirt-women-s-grey-heather-front.jpg' },
      { id: 2, categoryId: 2, name: 'Polos', priceRange: '$24.99 - $149.99', description: 'Polos from the 2017 Collection', imageUrl: 'https://i.pinimg.com/736x/94/05/0e/94050ecdb82b78833df2bdd886e7858a--slow-fashion-preppy-fashion.jpg' },
      { id: 3, categoryId: 3, name: 'T-Shirt', priceRange: '$88.99 - $144.99', description: 'T-Shirt from the 2017 Collection', imageUrl: 'https://ae01.alicdn.com/kf/HTB1vOeVIVXXXXa2XXXXq6xXFXXXf/Off-shoulder-t-shirt-women-tshirt-cotton-camisetas-feminina-2016-vetement-femme-plus-size-t-shirt.jpg_640x640.jpg' },
      { id: 4, categoryId: 4, name: 'Short', priceRange: '$78.99 - $148.99', description: 'Short from the 2017 Collection', imageUrl: 'http://g03.a.alicdn.com/kf/HTB1XyWGIXXXXXX9XVXXq6xXFXXXZ/Summer-Style-Sexy-font-b-White-b-font-font-b-Jumpsuit-b-font-Women-font-b.jpg' },
      { id: 5, categoryId: 1, name: 'Tank Top', priceRange: '$8.99 - $14.99', description: 'Sexy Tank Top from the 2017 Collection', imageUrl: 'https://ae01.alicdn.com/kf/HTB1GO7KJpXXXXXrXVXXq6xXFXXXM/2017-Summer-font-b-Women-b-font-font-b-Tank-b-font-font-b-Tops-b.jpg' },
      { id: 6, categoryId: 2, name: 'Push Up Bra', priceRange: '$8.99 - $14.99', description: 'Sexy Push Up Bra from the 2017 Collection', imageUrl: 'http://image.11st.my/g3/4/5/2/6/1/2/9452612_A1_V1.jpg' },
      { id: 7, categoryId: 3, name: 'Track Suit', priceRange: '$8.99 - $14.99', description: 'Track Suit from the 2017 Collection', imageUrl: 'https://ae01.alicdn.com/kf/HTB1EKn7PVXXXXXVXXXXq6xXFXXXz/Hot-Sale-Lady-Tracksuit-font-b-Women-b-font-Sweatshirt-Pant-font-b-Track-b-font.jpg' },
      { id: 8, categoryId: 4, name: 'Jump Suit', priceRange: '$98.99 - $149.99', description: 'Sexy Jump Suit from the 2017 Collection', imageUrl: 'https://ae01.alicdn.com/kf/HTB1PQIlHFXXXXXQXVXXq6xXFXXXX/Summer-Fashion-Navy-Blue-White-Short-Jumpsuit-Women-Elegant-Half-Sleeve-Casual-Chiffon-Jumpsuits.jpg' },
      { id: 9, categoryId: 1, name: 'Jacket', priceRange: '$89.99 - $199.99', description: 'Sexy Jacket from the 2017 Collection', imageUrl: 'https://s-media-cache-ak0.pinimg.com/736x/0c/d3/d4/0cd3d453019498e976c3b6f09e292137--womens-blazers--jackets-fall-jackets.jpg' },
      { id: 10, categoryId: 2, name: 'Shoe', priceRange: '$114.99 - $149.99', description: 'Quality Shoe from the 2017 Collection', imageUrl: 'https://s-media-cache-ak0.pinimg.com/originals/95/68/7a/95687ad2e168b1e5639a1dbfb8bb8418.jpg' },
      { id: 11, categoryId: 3, name: 'Sandals', priceRange: '$88.99 - $141.99', description: 'Sandals from the 2017 Collection', imageUrl: 'https://www.dhresource.com/0x0/f2/albu/g2/M00/21/31/rBVaGlVUpP2AJVFoAAIYnOUxPGw675.jpg' },
      { id: 12, categoryId: 4, name: 'Long Dress', priceRange: '$89.99 - $109.99', description: 'Long Dress from the 2017 Collection', imageUrl: 'http://www.gravetics.com/wp-content/uploads/2017/01/PRINT-SUMMER-MAXI-DRESSES.jpg' },
      { id: 13, categoryId: 1, name: 'Luxury Watch', priceRange: '$99.99 - $999.99', description: 'Luxury Watch from the 2017 Collection', imageUrl: 'http://sc02.alicdn.com/kf/HTB1dr3cSpXXXXaQapXXq6xXFXXXw/Fashion-Women-Watch-Luxury-Unique-Stylish-Double.jpg' },
      { id: 14, categoryId: 2, name: 'Sneakers', priceRange: '$89.99 - $119.99', description: 'Sneakers from the 2017 Collection', imageUrl: 'https://i.pinimg.com/736x/3a/a1/66/3aa1667fc21ca4dc1956d978e5e4134c--black-sneakers-sneakers-women.jpg' },
      { id: 15, categoryId: 3, name: 'Eye Wear', priceRange: '$8.99 - $14.99', description: 'Sexy Eye Wear from the 2017 Collection', imageUrl: 'http://www.sheideas.com/wp-content/uploads/2016/03/Fancy-Ladies-Shades-Round-Design-Sunglasses.jpg' },
      { id: 16, categoryId: 4, name: 'Swimsuit', priceRange: '$8.99 - $14.99', description: 'Sexy Swimsuit from the 2017 Collection', imageUrl: 'https://target.scene7.com/is/image/Target/50674636?wid=328&hei=328&qlt=80&fmt=pjpeg' },
    ];

    const orders = [
      {id: 1, orderId: 'ORD982', item: 'T-Shirt', status: 'Pending', orderDate: new Date(), deliveryDate: new Date(), orderTotal: '$ 388.99' },
      {id: 2, orderId: 'ORD342', item: 'Sneakers', status: 'Shipped',  orderDate: new Date(), deliveryDate: new Date(), orderTotal: '$ 219.88' },
      {id: 3, orderId: 'OR1848', item: 'iPhone 6 Plus', status: 'Processing', orderDate: new Date(), deliveryDate: new Date(), orderTotal: '$ 777.88' },
      {id: 4, orderId: 'OR7429', item: 'Samsung Smart TV', status: 'Delivered', orderDate: new Date(), deliveryDate: new Date(), orderTotal: '$ 3000.88' },
      {id: 5, orderId: 'OR9842', item: 'Call of Duty IV', status: 'Shipped', orderDate: new Date(), deliveryDate: new Date(), orderTotal: '$ 100.88' },
      {id: 6, orderId: 'OR9843', item: 'Play Station Console', status: 'Pending', orderDate: new Date(), deliveryDate: new Date(), orderTotal: '$ 888.88' },
      {id: 7, orderId: 'OR9844', item: 'Shoe', status: 'Processing', orderDate: new Date(), deliveryDate: new Date(), orderTotal: '$ 888.88' },
    ];

    const categories = [
      {id: 1, name: "Women's Clothing & Accessories" },
      {id: 2, name: "Men's Clothing & Accessories" },
      {id: 3, name: "Shoes" },
      {id: 4, name: "Plus Size" },
    ];

    const notifications = [
      {id: 1, description: "Very long description here that may not fit into the page and may cause design problems"},
      {id: 2, description: "5 new members joined"},
      {id: 3, description: "5 new members joined"},
      {id: 4, description: "25 sales made"},
      {id: 5, description: "You changed your username"}
    ];

    const status = [
      {id: 1, name: 'Shipped'},
      {id: 2, name: 'Pending'},
      {id: 3, name: 'Delivered'},
      {id: 4, name: 'Processing'},
    ];
      return { products: products, orders: orders, categories: categories, notifications: notifications, status: status }
  }
}