import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import template from './profile.html';

@Component({
  selector: 'ab-profile',
  template: template
})
export class ProfileComponent implements OnInit {
  profile: any;

  constructor(public auth: AuthService) {}

  ngOnInit() {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }
}