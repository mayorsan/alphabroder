import { Component } from "@angular/core";
import template from "./settings.html";

@Component({
  selector: 'ab-settings',
  template: template
})
export class SettingsComponent {}