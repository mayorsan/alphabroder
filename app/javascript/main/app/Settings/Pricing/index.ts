import { Component } from "@angular/core";
import template from "./pricing.html";

@Component({
  selector: 'ab-pricing',
  template: template
})
export class PricingComponent {}