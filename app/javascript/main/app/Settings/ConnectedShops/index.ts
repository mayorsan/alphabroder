import { Component } from "@angular/core";
import template from "./connected-shops.html";

@Component({
  selector: 'ab-connected-shops',
  template: template
})
export class ConnectedShopsComponent {}