import { Component } from "@angular/core";
import template from "./subscription.html";

@Component({
  selector: 'ab-subscription',
  template: template
})
export class SubscriptionComponent {}