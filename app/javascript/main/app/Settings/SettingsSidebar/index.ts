import { Component } from "@angular/core";
import template from "./settings-sidebar.html";

@Component({
  selector: 'ab-settings-sidebar',
  template: template
})
export class SettingsSidebarComponent {}