import { NgModule } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';

import { AppComponent }           from './app.component';
import { HomeComponent }          from './Home/index';
import { DashboardComponent }     from './Dashboard/index';
import { MainProductComponent }   from './MainProduct/index';
import { OrderComponent }         from './Order/index';
import { ImportListComponent }    from './MainProduct/ImportList/index';
import { StoreProductComponent }  from './MainProduct/StoreProduct/index';
import { ProductDetailComponent } from './MainProduct/ProductDetail/index';
import { OrderDetailComponent }   from './Order/OrderDetail/index';

import { SettingsComponent }      from './Settings/index';
import { ConnectedShopsComponent }  from './Settings/ConnectedShops/index';
import { PricingComponent }       from './Settings/Pricing/index';
import { SubscriptionComponent }  from './Settings/Subscription/index';
import { ProfileComponent }       from './Settings/Profile/index';

import { NotificationsComponent } from './Notifications/index';
import { PageNotFoundComponent }  from './PageNotFound/index'; 

import { AuthGuardService as AuthGuard } from './services/auth-guard.service';


const APPROUTES: Routes = [
  { path: '',               component: HomeComponent },
  // { path: 'index',          component: HomeComponent },
  { path: 'dashboard',      component: DashboardComponent },
  { path: 'search',         component: MainProductComponent },
  { path: 'orders',         component: OrderComponent },
  { path: 'orders/:id',     component: OrderDetailComponent },
  { path: 'import-list',    component: ImportListComponent },
  { path: 'products',       component: StoreProductComponent },
  { path: 'products/:id',   component: ProductDetailComponent },
  { 
    path: 'settings', 
    component: SettingsComponent, 
    canActivate: [AuthGuard],
    children: 
    [
      { path: '',             component: ConnectedShopsComponent },
      { path: 'shops',        component: ConnectedShopsComponent },
      { path: 'pricing',      component: PricingComponent },
      { path: 'subscription', component: SubscriptionComponent },
      { path: 'profile',      component: ProfileComponent },
    ] 
  },
  { path: 'notifications',         component: NotificationsComponent},
  { path: 'not-found',             component: PageNotFoundComponent },
  { path: '**',                    redirectTo: '/not-found' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(APPROUTES)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {

}