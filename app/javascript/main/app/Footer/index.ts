import { Component, OnInit } from '@angular/core';
import footer from "./footer.html";

@Component({
  selector: 'ab-footer',
  template: footer
})
export class FooterComponent implements OnInit {
  
  constructor() {}

  ngOnInit() {}
}