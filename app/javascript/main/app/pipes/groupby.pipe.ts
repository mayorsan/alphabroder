import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
  name: 'groupBy'
})
export class GroupByPipe implements PipeTransform {
  transform(value: Array<any>, field: string): Array<any> {
    const groupedObj = value.reduce((previous, current) => {
      if(!previous[current[field]]) {
        previous[current[field]] = [current];
      } else {
        previous[current[field]].push(current);
      }
      return previous;
    }, {});
    return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
  }
}