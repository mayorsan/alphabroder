import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})

export class CapitalizePipe implements PipeTransform {
 
  transform(value: any, words: boolean): any {
    if (value) {
      if (words) {
      /*
        if words is true:
        \b = assert position at a word boundary
        \w = matches any word character (equal to [a-zA-Z0-9_])
        find the first letter of each word
        Ref - https://regex101.com/
      */
        value = value.toLowerCase();
        return value.replace(/\b\w/g, first => first.toLocaleUpperCase());
      } else {
        /*
         if words is false: 
         take character at position 0, first character
         and transform it to upper case, we then add '+'
         the remaining value of the string. slice(1) removed the first
         character off the string.
         Leaving us with the first character uppercase concatenated with the string minus the first character.
        */
        return value.charAt(0).toUpperCase() + value.slice(1);
      }
    }
    return value;
  }
}