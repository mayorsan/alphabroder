export interface Shop {
  id: number;
  name: string;
  email: string;
  domain: string;
  code: string;
}