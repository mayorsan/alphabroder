export class OptionValue {
  constructor(
    public id: number, 
    public name: string, // Small, Large, Medium
    public presentation: string
  ) {}
}
