export interface UserNotification {
  id: number;
  count?: number; 
  description?: string;  
}

