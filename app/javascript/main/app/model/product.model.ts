import { Image } from './image.model';
import { Category } from './category.model';
import { Status } from './status.model';

export interface Product {
    id: number;
    _id: Object;
    millSku: string;
    categoryId: number;
    name: Object; 
    millName: Object;
    colorName: Object;
    sizeName: Object;
    // priceRange: string; 
    description: Object; 
    images: any;
    // initialPrice?: number;
    // importListPrice?: number;
    // category?: Category;
    // inStock?: boolean;
    // amountInStock?: number;
    // sku?: string;
    // shippingStatus?: Status; 
    // orderStatus?: Status;
    // isChecked?: boolean;
    // images?: Image[];
}

