export class Image {
  constructor(
    public id: number, 
    public productId: number,
    public height: number, 
    public width: number,
    public productUrl: string,
    public url: string,
    public alt?: string,  
  ) {}
}
