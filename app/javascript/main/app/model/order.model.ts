import { Status } from './status.model';

export class Order {
  constructor(
    public id: number, 
    public orderId: string,
    public item: string, 
    public status: Status,
    public orderDate: Date,
    public deliveryDate: Date,
    public orderTotal: string,  
  ) {}
}
