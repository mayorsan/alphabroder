import { Component, OnInit } from '@angular/core';
// import { ActivatedRoute, ParamMap }    from '@angular/router';
import { AuthService } from '../services/auth.service';
// import { Shop } from '../model/shop.model';
import template from './dashboard.html';


@Component({
  selector: 'ab-dashboard',
  template: template
})
export class DashboardComponent implements OnInit {
  profile: any;
  constructor(
    // private route: ActivatedRoute,
    private auth: AuthService
  ) {
    auth.handleAuthentication();
  }

  ngOnInit(): void {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }

  login() {
    this.auth.login();
  }

  logout() {
    this.auth.logout()
  }
}

// To get last 5 orders using orders.slice(1, 5) - https://angular.io/tutorial/toh-pt5