import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import templateString from './template.html';

@Component({
  selector: 'ab-main',
  template: templateString
})
export class AppComponent implements OnInit {
  constructor(private auth: AuthService) {
    auth.handleAuthentication();
  }

  ngOnInit(): void {}
}
