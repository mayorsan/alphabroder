interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: '{7OxFkCmtaQIFAgTKxbegNmeReDbIIn7E}',
  domain: '{medicrib.auth0.com}',
  callbackURL: 'http://localhost:5000/callback'
};