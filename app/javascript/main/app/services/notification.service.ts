import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UserNotification } from '../model/notification.model'

@Injectable()
export class UserNotificationService {
  // private notificationsUrl = 'api/notifications';

  constructor(private http: Http) {}


  // getNotifications(): Promise<UserNotification[]> {
  //   return this.http.get(this.notificationsUrl)
  //               .toPromise()
  //               .then(response => response.json().data as UserNotification[])
  //               .catch(this.handleError);
  // }

  // write a method for notification count

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }
}