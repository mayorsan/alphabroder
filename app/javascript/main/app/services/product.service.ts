import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Product } from '../model/product.model';
import { Category } from '../model/category.model';


@Injectable()
export class ProductService {
  private products: Product[];
  private storeProducts: Product[] = [];
  private importList: Product[] = [];
  private product: Product;
  newImportListItem = new Subject<Product[]>();
  newStoreProductItem = new Subject<Product[]>();
  // private productsUrl = 'https://blankstyle.herokuapp.com/products';
  private productsUrl = 'http://localhost:5000/products'; // URL to web api 
  private queryUrl = '.json?keywords=';
  private categoriesUrl = 'api/categories';

  constructor(private http: Http) {}

  getProducts(page?: Number): Observable<Product[]> {
    return this.http.get(`${this.productsUrl}?page=${page}`)
                .map((response: Response) => <Product[]>response.json())
                  // .filter(<Product>(product) => product.images != null && product.images.AP != null && product.name != null && product.sku.AP != null ))
                .catch(this.handleError);
  }

  searchProducts(terms: Observable<string>) {
    return terms.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term));
  }

  searchEntries(term) {
    return this.http.get(this.productsUrl + this.queryUrl + term)
      .map(res => res.json());
  }

  getProduct(id: number): Observable<Product> {
    const url = `${this.productsUrl}/${id}`;
    return this.http.get(url)
                .map(response => response.json() as Product)
                .catch(this.handleError);
  }

  getCategories(): Promise<Category[]> {
    return this.http.get(this.categoriesUrl)
                .toPromise()
                .then(response => response.json().data as Category[])
                .catch(this.handleError);
  }

  // // To simulate a slow connection
  // getProductsSlowly(): Promise<Product[]> {
  //   return new Promise(resolve => {
  //     // Simulate server latency with 2 second delay
  //     setTimeout(() => resolve(this.getProducts()), 2000);
  //   });
  // }

  addProductToImportList(product: Product) {
    this.importList.push(product);
    this.newImportListItem.next(this.importList.slice());
  }

  pushProductToShop(product: Product) {
    this.storeProducts.push(product);
    this.newStoreProductItem.next(this.storeProducts.slice());
  }

  pushAllProductsToShop(products: Product[]) {
    this.storeProducts.push(...products);
    this.newStoreProductItem.next(this.storeProducts.slice());
  }

  removeProductFromImportList(product: Product) {
    //console.log(product.name + ' removed from import list');
    const position = this.importList.findIndex((item: Product) => {
      return item.id == product.id;
    });
    this.importList.splice(position, 1);
    //console.log(this.importList);
  }

  removeProductFromShop(product: Product) {
    const position = this.storeProducts.findIndex((item: Product) => {
      return item.id == product.id;
    });
    this.storeProducts.splice(position, 1);
  }

  removeAllProductsFromImportList() {
    return this.importList = [];
  }
  
  getImportLists() {
    // return a new array which is an exact copy of the one here 
    return this.importList.slice();
  }

  getStoreProducts() {
    return this.storeProducts.slice();
  }

  isProductOnImportList(product: Product) {
    return this.importList.find((prod: Product) => {
      return prod.id == product.id;
    });
  }

  isImportListEmpty() {
    if (this.importList.length <= 0) {
      return true;
    }
    else {
      return false;
    }
  }

  isProductInShop(product: Product) {
    return this.storeProducts.find((prod: Product) => {
      return prod.id == product.id;
    });
  }

  isStoreProductListEmpty() {
    if (this.storeProducts.length <= 0) {
      return true;
    } else {
      return false;
    }
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }
}