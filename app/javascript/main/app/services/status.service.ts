import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Status } from '../model/status.model'

@Injectable()
export class StatusService {
  status: Status; 
  private statusUrl = 'api/status';

  constructor(private http: Http) {}

  isShipped(name: string) {
    if (name === 'Shipped') {
      this.status.name = 'Shipped'
    }
  }
  getStatus(): Promise<Status[]> {
    return this.http.get(this.statusUrl)
                .toPromise()
                .then(response => response.json().data as Status[])
                .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }
}