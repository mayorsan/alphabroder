import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Order } from '../model/order.model';

@Injectable()
export class OrderService {
  // private ordersUrl = 'api/orders';

  constructor(private http: Http) {}


  getOrders(): Promise<Order[]> {
    return null;
  //   return this.http.get(this.ordersUrl)
  //               .toPromise()
  //               .then(response => response.json().data as Order[])
  //               .catch(this.handleError);
  }

  getOrder(id: number): Promise<Order> {
    return null;
  //   const url = `${this.ordersUrl}/${id}`;
  //   return this.http.get(url)
  //               .toPromise()
  //               .then(response => response.json().data as Order)
  //               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }
}