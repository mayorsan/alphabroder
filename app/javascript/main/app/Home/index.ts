import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import templateString from './home.html';

@Component({
  selector: 'ab-home',
  template: templateString
})
export class HomeComponent implements OnInit {

  constructor(private auth: AuthService) {
    auth.handleAuthentication();
  }

  ngOnInit(): void {}

  login() {
    this.auth.login();
  }

  logout() {
    this.auth.logout()
  }

  isAuthenticated() {
    this.auth.isAuthenticated();
  }

  test() {
    alert('test');
  }
}
