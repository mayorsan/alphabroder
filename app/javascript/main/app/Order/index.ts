import { Component, OnInit } from "@angular/core";
import { Router }            from '@angular/router';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order.model';
import template from "./order.html";

@Component({
  selector: 'ab-order',
  template: template
})
export class OrderComponent implements OnInit {
  orders: Order[];

  constructor(private orderService: OrderService,
              private router: Router) {}

  getOrders(): void {
    // this.orderService.getOrders()
    //   .then(orders => this.orders = orders);
  }

  ngOnInit(): void {
    this.getOrders();
  }

  filterOrders(order: Order) {
    this.router.navigate(['/orders'], 
        {queryParams: { orderDate: order.orderDate } });
  }
}