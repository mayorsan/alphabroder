import { Component, OnInit }    from '@angular/core';
// import { Http }      from "@angular/http";
import { ActivatedRoute, ParamMap }    from '@angular/router';
import { Location }                    from '@angular/common';
import { Order }                       from '../../model/order.model';
import { OrderService }                from '../../services/order.service';
import 'rxjs/add/operator/switchMap';
import template                        from './order-detail.html';

@Component({
  selector: 'ab-order-detail',
  template: template
})
export class OrderDetailComponent implements OnInit {
  order: Order;

  constructor(
    private orderService: OrderService,
    private route: ActivatedRoute, 
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getOrderRoute();
  }

  getOrderRoute() {
    this.route.paramMap
      .switchMap((params: ParamMap) => 
        this.orderService.getOrder(+params.get('id')))
      .subscribe(order => this.order = order);
  }

 
  goBack(): void {
    this.location.back();
  }
}