import { Component, OnInit } from '@angular/core';
import { UserNotificationService } from '../services/notification.service';
import { AuthService } from '../services/auth.service';
import { UserNotification } from '../model/notification.model';
import navbar from "./navbar.html";

@Component({
  selector: 'ab-navbar',
  template: navbar
})
export class NavbarComponent implements OnInit {

  notifications: UserNotification[];
  notificationCount: number;
  profile: any;
  
  constructor(
    private notificationService: UserNotificationService,
    private auth: AuthService
  ) {
    auth.handleAuthentication();
  }

  ngOnInit() {
    // this.getNotifications();
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }

  // getNotifications(): void {
  //   this.notificationService.getNotifications()
  //     .then(notifications => this.notifications = notifications);
  // }

  login() {
    this.auth.login();
  }

  logout() {
    this.auth.logout()
  }

  isAuthenticated() {
    this.auth.isAuthenticated();
  }
}