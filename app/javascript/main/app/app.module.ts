import { NgModule }               from '@angular/core';
import { BrowserModule }          from '@angular/platform-browser';
import { FormsModule }            from '@angular/forms';
//import { HttpClientModule }       from '@angular/common/http';
import { HttpModule }             from '@angular/http';
import { AppRoutingModule }       from './app-routing.module';

// Imports for loading & configuring the in-memory web api 
// import { InMemoryWebApiModule }   from 'angular-in-memory-web-api';
// import { InMemoryDataService }     from './in-memory-data.service';
// Finished, delete after API integration

import { NglModule }           from 'ng-lightning/ng-lightning';
import { SuiModule }           from 'ng2-semantic-ui';
import { NgxPaginationModule } from 'ngx-pagination'; 
import { NgxGalleryModule }    from 'ngx-gallery';

import { AppComponent }           from './app.component';
import { HomeComponent }          from './Home/index';
import { AuthNavbarComponent }    from './Authentication/AuthNavbar/index';
import { NavbarComponent }        from './NavBar/index';
import { SidebarComponent }       from './Sidebar/index';
import { DashboardComponent }     from './Dashboard/index';
import { MainProductComponent }   from './MainProduct/index';
import { StoreProductComponent }  from './MainProduct/StoreProduct/index';
import { ProductDetailComponent } from './MainProduct/ProductDetail/index';
import { ImportListComponent }    from './MainProduct/ImportList/index';
import { OrderComponent }         from './Order/index';
import { OrderDetailComponent }   from './Order/OrderDetail/index';
import { SettingsComponent }      from './Settings/index';
import { PricingComponent }       from './Settings/Pricing/index';
import { NotificationsComponent } from './Notifications/index';
import { SubscriptionComponent }  from './Settings/Subscription/index';
import { ProfileComponent }       from './Settings/Profile/index';
import { PageNotFoundComponent }  from './PageNotFound/index'; 
import { SettingsSidebarComponent } from './Settings/SettingsSidebar/index';
import { ConnectedShopsComponent }  from './Settings/ConnectedShops/index';
import { FooterComponent }          from './Footer/index';
import { ProductService }           from './services/product.service';
import { OrderService }             from './services/order.service';
import { AuthService }              from './services/auth.service';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { UserNotificationService }      from './services/notification.service';
import { SearchPipe }               from './pipes/filter.pipe';
import { CapitalizePipe }           from './pipes/capitalize.pipe';
import { GroupByPipe }              from './pipes/groupby.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthNavbarComponent,
    NavbarComponent,
    SidebarComponent,
    DashboardComponent,
    MainProductComponent,
    StoreProductComponent,
    ProductDetailComponent,
    ImportListComponent,
    OrderComponent,
    OrderDetailComponent,
    SettingsComponent,
    SettingsSidebarComponent,
    ConnectedShopsComponent,
    PricingComponent,
    SubscriptionComponent,
    ProfileComponent,
    NotificationsComponent,
    PageNotFoundComponent,
    FooterComponent,
    SearchPipe,
    CapitalizePipe,
    GroupByPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
 //   HttpClientModule,
    // InMemoryWebApiModule.forRoot(InMemoryDataService),
    NglModule.forRoot(),
    SuiModule,
    NgxPaginationModule,
    NgxGalleryModule
  ],
  providers: [ 
    UserNotificationService,
    ProductService, 
    OrderService,
    AuthService,
    AuthGuard
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
