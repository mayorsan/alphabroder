import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import sidebar from "./sidebar.html";

@Component({
  selector: 'ab-sidebar',
  template: sidebar
})
export class SidebarComponent {
  profile: any;

  constructor(public auth: AuthService) {}

  ngOnInit() {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }
}