import { Component, OnInit, OnDestroy } from '@angular/core';
// import { Observable }        from 'rxjs/Observable'
import { Subscription }      from 'rxjs/Subscription';
import { Product }           from '../../model/product.model';
import { ProductService }    from '../../services/product.service';
import template              from './import-list.html';

@Component({
  selector: 'ab-import-list',
  template: template
})
export class ImportListComponent implements OnInit, OnDestroy {
  product: Product;
  importLists: Product[];
  storeProducts: Product[];
  selectedProducts: Product[];
  allowEdit: boolean = false;
  changesSaved: boolean = false;
  importListSubscription: Subscription;
  storeProductSubscription: Subscription;
  checked:boolean;
  
  constructor(
    private productService: ProductService
  ) {}
  
  ngOnInit(): void {
    this.getImportList();
    this.getStoreProducts();
  }

  getImportList() {
    this.importLists = this.productService.getImportLists();
    this.importListSubscription = this.productService.newImportListItem.subscribe((importLists: Product[]) => {
      this.importLists = importLists;
    });
  }

  // cloneImportList(obj) {
  //   if (Array.isArray(obj)) {
  //     return Array.from(obj);
  //   } else {
  //     return Object.assign(Object.create(obj), obj);
  //   }
  // }

  getStoreProducts() {
    this.storeProducts = this.productService.getStoreProducts();
    this.storeProductSubscription = this.productService.newStoreProductItem.subscribe((storeProducts: Product[]) => {
      this.storeProducts = storeProducts
    });
  }

  onProductPushToShop(product: Product) {
    if (!this.isProductInShop(product)) {
      this.productService.pushProductToShop(product);
    }
    this.getStoreProducts();
  }

  onProductRemoveFromShop(product: Product) {
    this.productService.removeProductFromShop(product);
  }

  removeAllProducts() {
    this.productService.removeAllProductsFromImportList();
  }

  pushSelectedProductsToShop() {

  }

  removeSelectedProductsFromShop() {

  }

  countSelectedProducts() {
    // this.importLists.forEach(x => x.state = event.target.checked)
  }

  selectAllProducts() {
    // return this.importLists.every(_ => _.state);
  }

  pushAllProductsToShop() {
    this.productService.pushAllProductsToShop(this.importLists);
  }

  isProductInShop(product: Product) {
    return this.productService.isProductInShop(product);
  }

  importListIsEmpty() {
    return this.productService.isImportListEmpty();
  }

  ngOnDestroy() {
    this.importListSubscription.unsubscribe();
    this.storeProductSubscription.unsubscribe();
  }

}