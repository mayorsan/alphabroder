import { Component, OnInit, Input }        from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Product }                  from '../model/product.model';
import { Category }                 from '../model/category.model';
import { ProductService }           from '../services/product.service';
import 'rxjs/add/operator/filter';
import template                     from './main-product.html';

@Component({
  selector: 'ab-main-product',
  template: template
})
export class MainProductComponent implements OnInit {
  // passing something out, use @Output
  // passing something in, use @Input
  page: number;
  total: number;
  products: Product[];
  categories: Category[];
  category: Category;
  results: Object;
  searchTerm$ = new Subject<string>();
  errorMessage: string;

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getProducts(this.page);
    
    // this.getCategories();
    // this.activatedRoute.queryParams.subscribe(params => {
    //     this.getProducts(Number(params['category']));
    //   });
  }

  // Products from the API 
  // getProducts(categoryId?: number): void {
  //   if (categoryId) {
  //     this.productService.getProducts()
  //       .then(products => {
  //         this.products = products.filter((product: Product) => product.categoryId === categoryId);
  //       });
  //   } else {
  //   this.productService.getProducts()
  //     .then(products => this.products = products);
  //   }
  // }

  search($event) {
    this.productService.searchProducts(this.searchTerm$)
      .subscribe(products => {
        this.products = products;
        console.log(products);
      });
  }

  getProducts(page: number) {
    this.page = page;
    this.total = 2000;
    this.productService.getProducts(page)
      .subscribe(
        products => this.products = products,
        error => this.errorMessage = <any>error
      );
  }

  getCategories(): void {
    this.productService.getCategories()
      .then(categories => this.categories = categories);
  }

  filterProducts(category: Category) {
    this.router.navigate(['/search'], 
        {queryParams: { category: category.id } });
  }

  // Add/Push Product to ImportList
  onProductAdded(product: Product) {
    this.productService.addProductToImportList(product);
  }

  // Remove Product from ImportList
  onRemoveProductFromImpList(product: Product) {
    if (!this.isProductInShop) {
      this.productService.removeProductFromImportList(product);
    } else {
      this.productService.removeProductFromShop(product);
      this.productService.removeProductFromImportList(product);
    }
  }
  
  // Check if Product is on ImportList
  isProductOnImportList(product: Product) {
    return this.productService.isProductOnImportList(product);
  }

  isProductInShop(product: Product) {
    return this.productService.isProductInShop(product);
  }
}