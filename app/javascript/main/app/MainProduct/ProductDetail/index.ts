import { Component, OnInit }           from '@angular/core';
// import { Http }      from "@angular/http";
import { ActivatedRoute, ParamMap }    from '@angular/router';
import { Location }                    from '@angular/common';
import { 
  NgxGalleryOptions, 
  NgxGalleryImage,
  NgxGalleryAnimation } from 'ngx-gallery';
import { Product }                     from '../../model/product.model';
import { ProductService }              from '../../services/product.service';
import 'rxjs/add/operator/switchMap';
import template                        from './product-detail.html';

@Component({
  selector: 'ab-product-detail',
  template: template
})
export class ProductDetailComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  product: Product;
  products: Product[];
  similarProducts: Product[];
  errorMessage: string;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute, 
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getProductRoute();
    this.getSimilarProduct();
    this.getGalleryOptions();
    this.galleryImages = [];
  }

  getGalleryOptions() {
    this.galleryOptions = [
      {
        width: '400px',
        height: '400px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      {
        breakpoint: 400,
        preview: false 
      }
    ]
  }

  getProductRoute() {
    this.route.paramMap
      .switchMap((params: ParamMap) => 
        this.productService.getProduct(+params.get('id')))
      .subscribe(product => {
        this.product = product;

        this.galleryImages = [{
          small: product.images.AP.back,
          medium: product.images.AP.front,
          big: product.images.AP.front
        }]
      });
  }

  getSimilarProduct() {
    this.productService.getProducts()
      .subscribe(
        products => this.similarProducts = products.slice(0, 4),
        error => this.errorMessage = <any>error);
  }

  onProductAdded(product: Product) { // Add an Item to ImportList
   // after some logic add product to ImportList
    this.productService.addProductToImportList(product);
  }

  onRemoveProductFromImpList(product: Product) {
    if (!this.isProductInShop) {
      this.productService.removeProductFromImportList(product);
    } else {
      this.productService.removeProductFromShop(product);
      this.productService.removeProductFromImportList(product);
    }
  }

  isProductOnImportList(product: Product) {
    return this.productService.isProductOnImportList(product);
  }

  isProductInShop(product: Product) {
    return this.productService.isProductInShop(product);
  }
  
  goBack(): void {
    this.location.back();
  }
}