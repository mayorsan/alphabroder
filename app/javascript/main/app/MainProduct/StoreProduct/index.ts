import { Component, OnInit } from "@angular/core";
import { Product }           from '../../model/product.model';
import { ProductService }    from '../../services/product.service';
import template              from "./store-product.html";

@Component({
  selector: 'ab-store-product',
  template: template
})
export class StoreProductComponent implements OnInit {
  storeProducts: Product[];
  // to use HTTPCLIENT in the future - https://medium.com/codingthesmartway-com-blog/angular-4-3-httpclient-accessing-rest-web-services-with-angular-2305b8fd654b
  constructor(private productService: ProductService) {}

  ngOnInit(): void {
   this.storeProducts = this.productService.getStoreProducts();
    this.productService.newStoreProductItem.subscribe((storeProducts: Product[]) => {
      this.storeProducts = storeProducts
    });
  }

  onProductRemoveFromShop(product: Product) {
    this.productService.removeProductFromShop(product);
    this.storeProducts = this.productService.getStoreProducts();
  }

  storeProductListEmpty() {
    return this.productService.isStoreProductListEmpty();
  }
}