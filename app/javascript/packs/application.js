/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

import './css/AdminLTE.css';
import './css/_all-skins.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'semantic-ui-sass/semantic-ui.scss';
import './css/application.css';

import 'semantic-ui-sass/semantic-ui.js';
import 'bootstrap/dist/js/bootstrap.js';
import 'hammerjs';
import './js/adminlte.js';
import 'auth0-js/build/auth0.js';

console.log('Hello World from Webpacker')
