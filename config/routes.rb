Rails.application.routes.draw do 
  root to: "dashboard#index"
  mount ShopifyApp::Engine, at: '/'
  resources :products
  resources :prices
  resources :inventories
  resources :trackings
  
  get '/callback', to: 'dashboard#callback'
  get 'dashboard/ng', to: 'dashboard#ng'
  get 'dashboard/ng/*angular_route', to: 'dashboard#ng'

  # Just to see what's happening
  namespace :examples do
    get "/shopify/auth", to: "shopify#auth"
  end
end
