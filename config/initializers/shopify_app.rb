ShopifyApp.configure do |config|
  config.application_name = "ABCD"
  config.api_key = ENV['SHOPIFY_CLIENT_API_KEY']
  config.secret = ENV['SHOPIFY_CLIENT_API_SECRET']
  config.scope = "read_orders, write_orders, read_products, write_products, read_product_listings, read_collection_listings, read_fulfillments, write_fulfillments, read_shipping, write_shipping, read_checkouts, write_checkouts"
  config.embedded_app = false
  config.after_authenticate_job = false
  config.session_repository = Shop
end
