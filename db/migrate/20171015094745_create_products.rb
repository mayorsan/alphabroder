class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.jsonb  :_id
      t.jsonb  :name
      t.jsonb  :description
      t.jsonb  :itemNumber
      t.jsonb  :sku
      t.jsonb  :images
      t.jsonb  :colorName
      t.jsonb  :colorCode
      t.jsonb  :sizeName
      t.jsonb  :sizeCode
      t.jsonb  :weight
      t.jsonb  :millName
      t.jsonb  :colorHex
      t.jsonb  :swatchImage
      t.jsonb  :specs
      t.jsonb  :specsPdf
      t.jsonb  :extra
      t.jsonb  :related
      t.jsonb  :features
      t.jsonb  :mapPrice
      t.string :gtin
      t.string :millSku
      t.jsonb  :lastUpdated
    end
  end
end
