class AddIndexesToProducts < ActiveRecord::Migration[5.1]
  # https://stackoverflow.com/questions/34041821/add-index-on-jsonb-field
  # Understand varchar_patter_ops - https://www.postgresql.org/docs/9.5/static/indexes-opclass.html
  def change
    #unless index_exists?(:products, "name->'AP'", name: 'index_products_on_name_ap') 
      # remove_index :products, name: 'index_products_on_name_ap'
      # add_index :products, "name->'AP'", using: :gin, name: 'index_products_on_name_ap'
    #end
    #unless index_exists?(:products, "description->'AP'", name: 'index_products_on_description_ap')
      # remove_index :products, name: 'index_products_on_description_ap'
      # add_index :products, "description->'AP'", using: :gin, name: 'index_products_on_description_ap'
    #end
    #unless index_exists?(:products, "(sku->'AP')", name: 'index_products_on_sku_ap')
      # remove_index :products, name: 'index_products_on_sku_ap'
      # add_index :products, "(sku->'AP')", using: :gin, name: 'index_products_on_sku_ap'
    #end
    # add_index :products, "(products.colorName->'AP')", using: :gin, name: 'index_products_on_colorName_ap'
    # add_index :products, "(products.millName->'AP')", using: :gin, name: 'index_products_on_millName_ap'
  end
end

