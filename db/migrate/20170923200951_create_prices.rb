class CreatePrices < ActiveRecord::Migration[5.1]
  def change
    create_table :prices do |t|
      t.jsonb  :_id
      t.string :gtin
      t.string :type 
      t.string :vendor
      t.string :price 
      t.jsonb  :priceTrend
      t.jsonb  :startDate
      t.jsonb  :endDate
      t.jsonb  :lastUpdated
    end
  end
end
