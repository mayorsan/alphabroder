class CreateInventories < ActiveRecord::Migration[5.1]
  def change
    create_table :inventories do |t|
      t.jsonb  :_id
      t.string :gtin
      t.string :vendor
      t.jsonb  :warehouses
    end
  end
end
