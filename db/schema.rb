# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171025065455) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"
  enable_extension "fuzzystrmatch"
  enable_extension "btree_gin"

  create_table "inventories", force: :cascade do |t|
    t.jsonb "_id"
    t.string "gtin"
    t.string "vendor"
    t.jsonb "warehouses"
  end

  create_table "prices", force: :cascade do |t|
    t.jsonb "_id"
    t.string "gtin"
    t.string "type"
    t.string "vendor"
    t.string "price"
    t.jsonb "priceTrend"
    t.jsonb "startDate"
    t.jsonb "endDate"
    t.jsonb "lastUpdated"
  end

  create_table "products", force: :cascade do |t|
    t.jsonb "_id"
    t.jsonb "name"
    t.jsonb "description"
    t.jsonb "itemNumber"
    t.jsonb "sku"
    t.jsonb "images"
    t.jsonb "colorName"
    t.jsonb "colorCode"
    t.jsonb "sizeName"
    t.jsonb "sizeCode"
    t.jsonb "weight"
    t.jsonb "millName"
    t.jsonb "colorHex"
    t.jsonb "swatchImage"
    t.jsonb "specs"
    t.jsonb "specsPdf"
    t.jsonb "extra"
    t.jsonb "related"
    t.jsonb "features"
    t.jsonb "mapPrice"
    t.string "gtin"
    t.string "millSku"
    t.jsonb "lastUpdated"
    t.index "((sku ->> 'AP'::text))", name: "index_products_on_sku_ap", using: :gin
    t.index "lower((description ->> 'AP'::text))", name: "index_products_on_description_ap", using: :gin
    t.index "lower((name ->> 'AP'::text))", name: "index_products_on_name_ap", using: :gin
  end

  create_table "shops", force: :cascade do |t|
    t.string "shopify_domain", null: false
    t.string "shopify_token", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true
  end

  create_table "trackings", force: :cascade do |t|
    t.jsonb "_id"
    t.string "distributor"
    t.string "company_name"
    t.string "carrier"
    t.string "ship_from"
    t.string "box_number"
    t.string "customer_po"
    t.string "order_number"
    t.string "purchase_order_number"
    t.string "tracking_number"
    t.string "item_number"
    t.string "invoice_number"
    t.string "_created_on"
    t.string "color"
    t.string "inventory_key"
    t.string "description"
    t.string "size"
    t.string "invoice_attention"
    t.string "ship_via"
    t.string "size_id"
    t.string "style"
    t.string "customer_number"
    t.string "shipping_collect_number"
    t.string "shipping_carrier"
    t.string "invoice_date"
    t.string "order_status"
    t.string "po_number"
    t.string "terms"
    t.string "shipping_method"
    t.string "order_type"
    t.string "order_date"
    t.integer "order_id"
    t.integer "shipped_qty"
    t.integer "order_qty"
    t.integer "qty"
    t.integer "boxes_shipped"
    t.integer "total_cases"
    t.integer "freight"
    t.integer "total_pieces"
    t.integer "debit_credit"
    t.integer "sample_discount"
    t.integer "shipping"
    t.integer "lost_cash_discount"
    t.integer "small_order_fee"
    t.integer "total_boxes"
    t.integer "restock_fee"
    t.integer "cupon_discount"
    t.integer "total_lines"
    t.integer "set_up_fee"
    t.integer "cod"
    t.decimal "total"
    t.decimal "subTotal"
    t.decimal "subtotal"
    t.decimal "invoice_total"
    t.decimal "handling_fee"
    t.decimal "total_weight"
    t.decimal "tax"
    t.jsonb "last_updated"
    t.jsonb "shipment_date"
    t.jsonb "ship_date"
    t.jsonb "tracking_numbers"
    t.jsonb "_etag"
    t.jsonb "shipTo"
    t.jsonb "shipping_address"
    t.boolean "ship_blind"
    t.boolean "dropship"
  end

end
